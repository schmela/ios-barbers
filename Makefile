# Author: Michal Hradecky, xhrade08@stud.fit.vutbr.cz
# IOS Projekt 2: barbers
# 

CC=gcc                              
CFLAGS= -std=gnu99 -Wall -Wextra -lrt -Werror -pedantic

barbers: barbers.c
	$(CC) $(CFLAGS) barbers.c -o barbers
