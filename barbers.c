#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>

#include <sys/types.h>
#include <sys/wait.h>
#include <sys/shm.h>
#include <unistd.h>
#include <semaphore.h>
#include <signal.h>
#include <fcntl.h>
#include <sys/shm.h>

/*
Projekt 2: Barbers, IOS, 1BIT-A, 2010/2011
Autor: Michal Hradecky, 138497, xhrade08@stud.fit.vutbr.cz
 _                 _                     
| |               | |                    
| | _   ____  ____| | _   ____  ____ ___ 
| || \ / _  |/ ___) || \ / _  )/ ___)___)
| |_) | ( | | |   | |_) | (/ /| |  |___ |
|____/ \_||_|_|   |____/ \____)_|  (___/ 

*/

// sdilena struktura
typedef struct TShm
{
	int cekajici;
	int akce;
	int odbavenych;
	int dosouboru;
	FILE *soubor;
	int shm_id;
	int N;
} TShm;

// globalni deklarace semaforu
sem_t *ordinace, *odbaveni, *cislovani, *cekarna, *barb, *cust, *vzbuzeni;

// globalni sdilena promenna
TShm *sdilene;

// globalni promenna s pid vsech potomku
pid_t *pid_potomci;

// odchytavani signalu
struct sigaction signaly, signaly_p;

// jak muze dopadnout zpracovani parametru
enum
{
    PAR_OK = 1,
    //PAR_HELP = 0,
    PAR_ERR = -1,
    PAR_TOOLESS = -2,
    PAR_TOOMUCH = -3
};

// jak muzou dopadnout hlavni program
enum
{
    SUCCESS = 0,
    ERR_MEMORY = -1,
	ERR_FORK = -2,
	ERR_FTOK = -3,
	ERR_SHMGET = -4,
	ERR_SEM_OPEN = -5,
	ERR_FILE_OPEN = -6
};


// funkce zpracuje zadane parametry a podle toho vrati, co si uzivatel zada od programu, pripadne kod chyby, co nastala
int parse_parameters (int par_count,char *params[])
{
    if ( par_count < 6 ) return PAR_TOOLESS;
    if ( par_count == 6 ) 
	{
		// kontrola toho, zda jsou to ciselne parametry opravdu cisla
		char *endptr;
		long int pom=0;
		for (int i=1; i<5; i++)
		{
			pom=0;
			pom = strtol(params[i],&endptr,10);
			if ( params[i] + strlen(params[i]) > endptr ) return PAR_ERR;
			if ( pom < 0 ) return PAR_ERR;
		}
		return PAR_OK;
	}
    if ( par_count > 6 ) return PAR_TOOMUCH;
    return PAR_ERR;
}


// vytikne napovedu
void print_help (void)
{
    printf(
    "NAZEV:\n"
	"	 Projekt 2: Barbers, IOS, 1BIT-A, 2010/2011\n"
    "AUTOR:\n"
	"	 Autor: Michal Hradecky, 138497, xhrade08@stud.fit.vutbr.cz]\n"
	"POUZITI:\n"
	"    ./barbers Q GenC GenB N filename\n"
	"	Q - pocet zidli v cekarne\n"
	"	GenC - rozsah <0;GenC> [ms] pro generovani zakazniku\n"
	"	GenB - rozsah <0;GenB> [ms] pro generovani obsluhy\n"
	"	N - pocet vytvorenych zakazniku\n"
    );

}

void zavri_semafory()
{
	sem_close(ordinace);
	sem_close(odbaveni);
	sem_close(cislovani);
	sem_close(cekarna);
	sem_close(barb);
	sem_close(cust);
	sem_close(vzbuzeni);
}

void unlink_semafory()
{
	// odstraneni semaforu ze systemu
	sem_unlink("/sem_xhrade08_a");
	sem_unlink("/sem_xhrade08_b");
	sem_unlink("/sem_xhrade08_c");
	sem_unlink("/sem_xhrade08_d");
	sem_unlink("/sem_xhrade08_e");
	sem_unlink("/sem_xhrade08_f");
	sem_unlink("/sem_xhrade08_g");
}

void uklid_potomek (TShm *sdilene)
{
	// zavreni a odstraneni semaforu
	zavri_semafory();
	//unlink_semafory();

	// zavreni vystupniho souboru 
	if (sdilene != NULL)
	{
		if (sdilene->soubor != NULL)
		{
			fclose(sdilene->soubor);
			//sdilene->soubor=NULL;
			//printf ("uzavreno\n");
		}

		// dealokace sdilene pameti
		int pom=sdilene->shm_id;
		shmdt(sdilene);
		shmctl(pom, IPC_RMID, NULL);
		//sdilene = NULL;

		//free pid zakazniku
		free(pid_potomci);
	}

}
void uklid_rodic (TShm *sdilene)
{
	// zavreni a odstraneni semaforu
	zavri_semafory();
	unlink_semafory();

	// zavreni vystupniho souboru 
	if (sdilene != NULL)
	{
		if (sdilene->soubor != NULL)
		{
			fclose(sdilene->soubor);
			sdilene->soubor=NULL;
		}

		// dealokace sdilene pameti
		int pom=sdilene->shm_id;
		shmdt(sdilene);
		shmctl(pom, IPC_RMID, NULL);
		sdilene = NULL;
		free(pid_potomci);
	}
	
}

void sig_uklid_potomek (int sig)
{
	switch (sig)
	{
		case SIGINT: ;
			 break;
		case SIGTERM:
			{
				uklid_potomek(sdilene);
				exit(0);
				break;
			}
		default:;
	}


	//exit(0);
}

void sig_uklid_rodic (int sig)
{
	switch (sig)
	{
		case SIGINT: ;
		case SIGTERM:
			{
				for (int i=0; i<((sdilene->N)+1); i++)
				{
					if (pid_potomci[i]!=0)
					{
						kill(pid_potomci[i],SIGTERM);
						int status;
						waitpid(pid_potomci[i],&status,0);
						//printf("pid %d killed\n",pid_potomci[i]);
					}
				}

				uklid_rodic(sdilene);
				break;
			}
		default:;
	}


	exit(0);
}

/*
void hello (int sig)
{
	if (sig == SIGINT )
	{
		printf("SIGINT uklizim\n");
	}
	else
	{
		printf("SIGTERM uklizim \n");
	}
	switch (sig)
	{
		case SIGINT: ;
		case SIGTERM:
			{
				uklid_potomek(sdilene);
				break;
			}
		default:;
	}

	//kill (getpid(),SIGKILL);
	exit(0);
}
*/

void vypis_c (TShm *sdilene, int x, char *stav)
{
	if ( sem_wait(cislovani)== -1 )  kill(getppid(),SIGTERM);
	if (sdilene->dosouboru == 0)
		printf("%d: customer %d: %s\n",sdilene->akce,x,stav);
	else
		fprintf(sdilene->soubor,"%d: customer %d: %s\n",sdilene->akce,x,stav);
	(sdilene->akce)++;
	if ( sem_post(cislovani)== -1 )  kill(getppid(),SIGTERM);
}

void customer(int x,int zidli,TShm *sdilene)
{
	vypis_c(sdilene,x,"created");
	if ( sem_wait(cekarna)== -1 )  kill(getppid(),SIGTERM);
	if (sdilene->cekajici < zidli)
	{
		(sdilene->cekajici)++;
		if ( sem_post(cekarna)== -1 )  kill(getppid(),SIGTERM);
		vypis_c(sdilene,x,"enters");
		// vstup do ordinace, jestlize tam nikdo neni
		if ( sem_wait(ordinace)== -1 )  kill(getppid(),SIGTERM);
		// vzbuzeni barbera
		if ( sem_post(vzbuzeni)== -1 )  kill(getppid(),SIGTERM);
		// cekani na unlock od barbera
		if ( sem_wait(cust)== -1 )  kill(getppid(),SIGTERM);
		//sem_wait(vzbuzeni);
		vypis_c(sdilene,x,"ready");
		// unlock barbera
		if ( sem_post(barb)== -1 )  kill(getppid(),SIGTERM);
		// opet cekani na unlock od barbera
		if ( sem_wait(cust)== -1 )  kill(getppid(),SIGTERM);
		vypis_c(sdilene,x,"served");
		if ( sem_post(barb)== -1 )  kill(getppid(),SIGTERM);
		// odchod z ordinace
		if ( sem_post(ordinace)== -1 )  kill(getppid(),SIGTERM);
	}
	else
	{
		if ( sem_post(cekarna)== -1 )  kill(getppid(),SIGTERM);
		vypis_c(sdilene,x,"enters");
		vypis_c(sdilene,x,"refused");
		// zvednuti odbavenych
		if ( sem_wait(odbaveni)== -1 )  kill(getppid(),SIGTERM);
		(sdilene->odbavenych)++;
		if ( sem_post(odbaveni)== -1 )  kill(getppid(),SIGTERM);
	}
	if (zidli==0) 
	{
		//printf("0 zidli\n");
		if ( sem_post(vzbuzeni)== -1 )  kill(getppid(),SIGTERM);
	}
	
}

void vypis_b (TShm *sdilene, char *stav)
{
	if ( sem_wait(cislovani)== -1 )  kill(getppid(),SIGTERM);
	if (sdilene->dosouboru == 0)
		printf("%d: barber: %s\n",sdilene->akce,stav);
	else
		fprintf(sdilene->soubor,"%d: barber: %s\n",sdilene->akce,stav);
	(sdilene->akce)++;
	if ( sem_post(cislovani)== -1 )  kill(getppid(),SIGTERM);
}

void barber(int max_cekani, int celkem, TShm *sdilene)
{
	int checkovat = 1;
	if ( sem_wait(odbaveni) == -1 ) kill(getppid(),SIGTERM);
	while ( (sdilene->odbavenych) < celkem )
	{
		if ( sem_post(odbaveni)== -1 ) kill(getppid(),SIGTERM);
		if (checkovat == 1)
			vypis_b (sdilene, "checks");
		//printf("odbavenych %d  v cekarne %d\n",sdilene->odbavenych,sdilene->cekajici);
		if ( sem_wait(cekarna)== -1 ) kill(getppid(),SIGTERM);
		if ( sdilene->cekajici>0 )
		{
			vypis_b(sdilene,"ready");
			(sdilene->cekajici)--;
			if ( sem_post(cekarna)== -1 ) kill(getppid(),SIGTERM);
			checkovat = 1;
		/////////////////////////////////	
	//		if (sdilene->odbavenych > 5) kill(getppid(),SIGTERM);
		////////////////////////////////
			//unlock zakaznik
				if ( sem_post(cust)== -1 ) kill(getppid(),SIGTERM);
			//cekani na unlock od zakaznika
				if ( sem_wait(barb)== -1 ) kill(getppid(),SIGTERM);

			// simulace strihani
			int doba=1000*(rand() % (max_cekani+1));
			usleep(doba);


			vypis_b(sdilene,"finished");
			// opet unlock zakaznika
			if ( sem_post(cust)== -1 ) kill(getppid(),SIGTERM);
			if ( sem_wait(barb)== -1 ) kill(getppid(),SIGTERM);
			if ( sem_wait(odbaveni)== -1 ) kill(getppid(),SIGTERM);
			(sdilene->odbavenych)++;
			if ( sem_post(odbaveni)== -1 ) kill(getppid(),SIGTERM);
			if ( sem_wait(odbaveni)== -1 ) kill(getppid(),SIGTERM);
		//	printf("odbavenych %d  v cekarne %d\n",sdilene->odbavenych,sdilene->cekajici);
		}
		else
		{
			if ( sem_post(cekarna)== -1 ) kill(getppid(),SIGTERM);
			checkovat = 0;
			//printf("--1--\n");
			//barber usne
			if ( sem_wait(vzbuzeni)== -1 ) kill(getppid(),SIGTERM);
			//printf("--2--\n");
			if ( sem_wait(odbaveni)== -1 ) kill(getppid(),SIGTERM);
			//printf("--3--\n");
		}
	}
	//printf("barber end\n");
}

int Barbery(char *params[])
{

	// zpracovani jiz overenych parametru
	int Q = strtol(params[1],NULL,10); //pocet zidli v cekarne
	int GenC = strtol(params[2],NULL,10); // doba mezi generaci zakazniku
	int GenB = strtol(params[3],NULL,10); // doba strihani
	int N = strtol(params[4],NULL,10); // celkovy pocet zakazniku

	// kdyz je 0 zakazniku, tak se rovnou skonci	
	if ( N==0 ) return SUCCESS;

	// signaly
	signaly.sa_flags = 0;
	sigemptyset (&signaly.sa_mask);
	sigaddset(&signaly.sa_mask,SIGINT);
	//sigaddset(&signaly.sa_mask,SIGTERM);
	signaly.sa_handler = sig_uklid_rodic;
	sigaction (SIGTERM, &signaly, 0);
	sigaction (SIGINT, &signaly, 0);

	// sdilena pamet pro celou strukturu TShm
	key_t klic;
	int shm_id;
	if ( ( klic=ftok(params[0],99)) == -1 ) return ERR_FTOK; 
	if ( ( shm_id=shmget(klic,sizeof(TShm),IPC_CREAT | 0666 )) < 0 ) return ERR_SHMGET;
	sdilene = NULL;
	sdilene = (TShm *) shmat(shm_id,NULL,0);
	if (sdilene == (void *) -1) return ERR_SHMGET;
	sdilene->cekajici=0;
	sdilene->akce=1;
	sdilene->odbavenych=0;
	sdilene->soubor=NULL;
	sdilene->shm_id = shm_id;
	sdilene->N=N;

	// inicializace semaforu
	vzbuzeni = sem_open("/sem_xhrade08_a", O_CREAT, 0666, 0);
	if ( vzbuzeni == SEM_FAILED ) { uklid_rodic (sdilene); return ERR_SEM_OPEN;}
	ordinace = sem_open("/sem_xhrade08_b", O_CREAT, 0666, 1);
	if ( ordinace == SEM_FAILED ) { uklid_rodic (sdilene); return ERR_SEM_OPEN;}
	cekarna = sem_open("/sem_xhrade08_c", O_CREAT, 0666, 1);
	if ( cekarna == SEM_FAILED )  { uklid_rodic (sdilene); return ERR_SEM_OPEN;}
	cislovani = sem_open("/sem_xhrade08_d", O_CREAT, 0666, 1);
	if ( cislovani == SEM_FAILED )  { uklid_rodic (sdilene); return ERR_SEM_OPEN;}
	odbaveni = sem_open("/sem_xhrade08_e", O_CREAT, 0666, 1);
	if ( odbaveni == SEM_FAILED )  { uklid_rodic (sdilene); return ERR_SEM_OPEN;}
	barb = sem_open("/sem_xhrade08_f", O_CREAT, 0666, 0);
	if ( barb == SEM_FAILED )  { uklid_rodic (sdilene); return ERR_SEM_OPEN;}
	cust = sem_open("/sem_xhrade08_g", O_CREAT, 0666, 0);
	if ( cust == SEM_FAILED )  { uklid_rodic (sdilene); return ERR_SEM_OPEN;}

	if ( (strcmp(params[5],"-"))!=0 )
	{
		// zalozi se soubor
		if ( (sdilene->soubor=fopen(params[5],"w")) == NULL )
		{
			uklid_rodic (sdilene);
			return ERR_FILE_OPEN;
		}
		setvbuf(sdilene->soubor, (char *)NULL, _IONBF, 0);
		sdilene->dosouboru=1;
		
	}
	else
	{
		sdilene->soubor = NULL;
		sdilene->dosouboru=0;
	}

	// cast, kde se spousti procesy			
	int i=0;
	//pid_t pid_potomci[N+1];
	pid_potomci = malloc((N+1)*sizeof(pid_t));
	if ( pid_potomci == NULL ) 
	{
		uklid_rodic(sdilene);
	}
	for(int j=0; j<(N+1); j++)
	{
		pid_potomci[j]=0;
	}

	pid_potomci[0]=fork();
	if ( pid_potomci[0]==-1 )
	{
		// znici vsechny potomky
		for (int j=0; j < i;j++)
		{
			kill(pid_potomci[j],SIGTERM);
		}
		uklid_rodic(sdilene);
		return ERR_FORK;
	}
	else 
		if ( pid_potomci[0]==0 )
		// fork klon pro holice
		{
			// chytani signalu pro potomka
			signaly_p.sa_flags = 0;
			sigemptyset (&signaly_p.sa_mask);
			sigaddset(&signaly_p.sa_mask,SIGINT);
			signaly_p.sa_handler = sig_uklid_potomek;
			sigaction (SIGTERM, &signaly_p, 0);
			sigaction (SIGINT, &signaly_p, 0);

			barber(GenB,N,sdilene);
			
			// zavreni semaforu
			zavri_semafory();

			// zavreni souboru
			if (sdilene->dosouboru == 1)
				fclose(sdilene->soubor);
				
			// dealokace sdilene pameti
			shmdt(sdilene);
			shmctl(shm_id, IPC_RMID, NULL);

			//free pid zakazniku
			free(pid_potomci);
		}
		else
		// puvodni proces
		{
			// vytvori postupne N zakazniku
			srand(time(NULL));
			for ( i=1;i<(N+1);i++ )
			{
				pid_potomci[i]=fork();
				if ( pid_potomci[i]==-1 ) 
				{
					// znici vsecny potomky
					for (int j=0; j < i;j++)
					{
						int status;
						kill(pid_potomci[j],SIGTERM);
						waitpid (pid_potomci[j],&status,0);
					}
					uklid_rodic(sdilene);
					return ERR_FORK;
				}
				else 
					if ( pid_potomci[i]==0 )
					{
						// chytani signalu pro potomka
						signaly_p.sa_flags = 0;
						sigemptyset (&signaly_p.sa_mask);
						sigaddset(&signaly_p.sa_mask,SIGINT);
						signaly_p.sa_handler = sig_uklid_potomek;
						sigaction (SIGTERM, &signaly_p, 0);
						sigaction (SIGINT, &signaly_p, 0);

						// spusti se kod pro zakaznika
						customer(i,Q,sdilene);
						
						// dealokace semaforu
						zavri_semafory();

						// zavreni souboru
						if (sdilene->dosouboru == 1)
							fclose(sdilene->soubor);
						
						// dealokace sdilene pameti
						shmdt(sdilene);
						shmctl(shm_id, IPC_RMID, NULL);

						//free pid zakazniku
						free(pid_potomci);

						return SUCCESS;

					}
					else
					{
						// cekani mezi vytvarenim zakazniku
						int doba=1000*(rand() % (GenC+1));
						usleep(doba);				
					}
			}
			
			// cekani nez se ukonci vsichni zakaznici a holic
			int status, exitstatus;
			pid_t w;
			for (int j=1; j<(N+1); j++)
			{
				//w = waitpid(pid_potomci[j],&status,0);
				if ( (w = waitpid(pid_potomci[j],&status,0)) == -1 ) kill(getpid(),SIGTERM);				
				if ( (exitstatus=WEXITSTATUS(status))!= 0 ) kill(getpid(),SIGTERM);

			}
			// cekani na holice
			if ( (w = waitpid(pid_potomci[0],&status,0)) == -1 ) kill(getpid(),SIGTERM);				
			if ( (exitstatus=WEXITSTATUS(status))!= 0 ) kill(getpid(),SIGTERM);

			// zabiti holice
			//kill(pid_holic,9);

			// zavreni a odstraneni semaforu ze systemu
			sem_close(ordinace);
			sem_unlink("/sem_xhrade08_a");
			sem_close(odbaveni);
			sem_unlink("/sem_xhrade08_b");
			sem_close(cislovani);
			sem_unlink("/sem_xhrade08_c");
			sem_close(cekarna);
			sem_unlink("/sem_xhrade08_d");
			sem_close(barb);
			sem_unlink("/sem_xhrade08_e");
			sem_close(cust);
			sem_unlink("/sem_xhrade08_f");
			sem_close(vzbuzeni);
			sem_unlink("/sem_xhrade08_g");

			// zavreni souboru
			if (sdilene->dosouboru == 1)
				fclose(sdilene->soubor);

			// dealokace sdilene pameti
			shmdt(sdilene);
			shmctl(shm_id, IPC_RMID, NULL);

			//free pid zakazniku
			free(pid_potomci);

		}

	return SUCCESS;
}

/*                _
  _ __ ___   __ _(_)_ __
 | '_ ` _ \ / _` | | '_ \
 | | | | | | (_| | | | | |
 |_| |_| |_|\__,_|_|_| |_|

*/

int main(int argc,char *argv[])
{
    int main_errors = SUCCESS;
    int rezim = parse_parameters(argc,argv); // jake jsou parametry >1 normal 0 help <0 error

        switch (rezim)
        {
            // NORMALNI FUNKCE
            case PAR_OK:
            {
				//randomize();
				//printf("%d hlavni program \n",pid);				
				main_errors = Barbery(argv);					
				break;
            }
            // ERRORY A HELP
/*
            case PAR_HELP:
                print_help();
                return EXIT_SUCCESS;
                break;*/
            case PAR_ERR:
                fprintf(stderr,"Error: Spatne zadane parametry.\n");
                return EXIT_FAILURE;
                break;
            case PAR_TOOLESS:
                fprintf(stderr,"Error: Malo parametru.\n");
                return EXIT_FAILURE;
                break;
            case PAR_TOOMUCH:
                fprintf(stderr,"Error: Moc parametru.\n");
                return EXIT_FAILURE;
                break;
            default:
                return EXIT_FAILURE;

        }


    switch (main_errors)
    {
        case SUCCESS:
            return EXIT_SUCCESS;
            break;
        case ERR_MEMORY:
            fprintf(stderr,"Error: Chyba pameti\n");
            return EXIT_FAILURE;
            break;
		case ERR_FORK:
            fprintf(stderr,"Error: Chyba pri fork()\n");
            return EXIT_FAILURE;
            break;
		case ERR_FTOK:
            fprintf(stderr,"Error: Chyba pri ftok()\n");
            return EXIT_FAILURE;
            break;
		case ERR_SHMGET:
            fprintf(stderr,"Error: Chyba pri praci se sdilenou pameti\n");
            return EXIT_FAILURE;
            break;
		case ERR_SEM_OPEN:
            fprintf(stderr,"Error: Chyba pri sem_open()\n");
            return EXIT_FAILURE;
            break;
		case ERR_FILE_OPEN:
            fprintf(stderr,"Error: Chyba pri otvirani vystupniho souboru\n");
            return EXIT_FAILURE;
            break;
			
    }
}
